Bicharka
======

Get Started
-----------

#### Requirements

To run this application on your machine, you need at least:

* >= PHP 5.4
* Nginx Web Server
* Phalcon Framework 2.0 extension installed/enabled 
* MySQL >= 5.1.5

Then you'll need to create the database and initialize schema:

    echo 'CREATE DATABASE bicharka' | mysql -u root
    cat schemas/bicharka.sql | mysql -u root bicharka

Installing Dependencies via Composer
------------------------------------
Bicharka's dependencies must be installed using Composer. Run the composer installer:

```bash
cd bicharka
composer install
```


**IMPORTANT: Theme is only for development purpose on local environment. If you go to production please consider theme usage conditions in public directory. I'm not responsible for braking any usage conditions. Thank you for understanding.**