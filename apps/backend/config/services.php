<?php

use Bicharka\Common\Library\Acl\Acl;
use Bicharka\Common\Library\Auth\Auth;
use Bicharka\Common\Library\Mail\Mail;
use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\View;
use Phalcon\Crypt;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Assets\Manager as AssetsManager;
use Phalcon\Mvc\Model\Metadata\Files as MetaDataAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Flash\Direct as Flash;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Logger\Formatter\Line as FormatterLine;

/**
 * Register the global configuration as config
 */
$config = $this->config;

$di->set('config', $config);

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->set('url', function () use ($config) {
  $url = new UrlResolver();
  $url->setBaseUri($config->application->baseUri);

  return $url;
}, true);

/**
 * Assets manager
 */
$di->setShared('assets', function () {
  return new AssetsManager();
});

/**
 * Memcached cache
 */
$di->setShared('memcache', function () {
  return new Memcached();
});

/**
 * Setting up the view component
 */
$di->setShared('view', function () use ($config) {
  $view = new View();
  $view->setViewsDir($config->application->viewsDir);

  return $view;
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('db', function () use ($config) {
  return new DbAdapter(array(
    'host' => $config->database->host,
    'username' => $config->database->username,
    'password' => $config->database->password,
    'dbname' => $config->database->dbname,
  ));
});

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->set('modelsMetadata', function () use ($config) {
  return new MetaDataAdapter(array(
    'metaDataDir' => $config->application->cacheDir . 'metaData/',
  ));
});

/**
 * Start the session the first time some component request the session service
 */
$di->set('session', function () {
  $session = new SessionAdapter();
  $session->start();

  return $session;
});

/**
 * Crypt service
 */
$di->set('crypt', function () use ($config) {
  $crypt = new Crypt();
  $crypt->setKey($config->application->cryptSalt);

  return $crypt;
});

/**
 * Dispatcher use a default namespace
 */
$di->set('dispatcher', function () use ($di) {
  $eventsManager = $di->getShared('eventsManager');
  $eventsManager->attach('dispatch:afterException', function ($event, $dispatcher, $exception) {
    switch ($exception->getCode()) {
      case Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
      case Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
        $dispatcher->forward([
            'controller' => 'index',
            'action' => 'error404',
          ]
        );

        return false;
        break;
      default:
        $dispatcher->forward([
            'controller' => 'index',
            'action' => '500',
          ]
        );

        return false;
        break;
    }
  }
  );

  $dispatcher = new Dispatcher();
  $dispatcher->setEventsManager($eventsManager);
  $dispatcher->setDefaultNamespace('Bicharka\Backend\Controllers');

  return $dispatcher;
});

/**
 * Loading routes from the router.php file
 */
$di->set('router', function () {
  return require __DIR__ . '/router.php';
});

/**
 * Flash service with custom CSS classes
 */
$di->set('flash', function () {
  return new Flash(array(
    'error' => 'alert alert-danger',
    'success' => 'alert alert-success',
    'notice' => 'alert alert-info',
    'warning' => 'alert alert-warning',
  ));
});

/**
 * Custom authentication component
 */
$di->set('auth', function () {
  return new Auth();
});

/**
 * Mail service uses AmazonSES
 */
$di->set('mail', function () {
  return new Mail();
});

/**
 * Access Control List
 */
$di->set('acl', function () {
  $cacheFilePath = '/backend/cache/acl/data.txt';
  return new Acl($cacheFilePath);
});


/**
 * Logger service
 */
$di->set('logger', function ($filename = null, $format = null) use ($config) {
  $format = $format ?: $config->get('logger')->format;
  $filename = trim($filename ?: $config->get('logger')->filename, '\\/');
  $path = rtrim($config->get('logger')->path, '\\/') . DIRECTORY_SEPARATOR;

  $formatter = new FormatterLine($format, $config->get('logger')->date);
  $logger = new FileLogger($path . $filename);

  $logger->setFormatter($formatter);
  $logger->setLogLevel($config->get('logger')->logLevel);

  return $logger;
});
