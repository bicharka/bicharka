<?php

use Phalcon\Config;
use Phalcon\Logger;

return [
    'application' => [
        'controllersDir' => APP_DIR . '/backend/controllers/',
        'modelsDir' => APP_DIR . '/backend/models/',
        'formsDir' => APP_DIR . '/backend/forms/',
        'viewsDir' => APP_DIR . '/backend/views/',
        'cacheDir' => APP_DIR . '/backend/cache/',
        'baseUri' => '/',
    ],
    'logger' => [
        'path'     => APP_DIR . '/backend/logs/',
        'format'   => '%date% [%type%] %message%',
        'date'     => 'D j H:i:s',
        'logLevel' => Logger::DEBUG,
        'filename' => 'app_backend.log',
    ]
];
