<?php

namespace Bicharka\Backend\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Email;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email as EmailValidator;
use Phalcon\Validation\Validator\Identical;

class LoginForm extends Form {

  public function initialize () {
    $email = new Email('email', [
      'placeholder' => 'Email',
    ]);
    $email->addValidators([
      new PresenceOf([
        'message' => 'The e-mail is required',
      ]),
      new EmailValidator([
        'message' => 'The e-mail is not valid',
      ]),
    ]);
    $this->add($email);

    // Password
    $password = new Password('password', [
      'placeholder' => 'Password',
    ]);
    $password->addValidator(new PresenceOf([
      'message' => 'The password is required',
    ]));
    $password->clear();
    $this->add($password);

    // Remember
    $remember = new Check('remember', [
      'value' => 'yes',
    ]);
    $remember->setLabel('Remember Me');
    $this->add($remember);

    // CSRF
    $csrf = new Hidden('csrf');
    $csrf->addValidator(new Identical([
      'value' => $this->security->getSessionToken(),
      'message' => 'CSRF validation failed',
    ]));
    $csrf->clear();
    $this->add($csrf);
  }
}
