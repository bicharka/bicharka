<?php

namespace Bicharka\Common\Models;

use Phalcon\Mvc\Model;

/**
 * Bicharka\Models\Profile
 *
 * All the profile levels in the application. Used in conjunction with ACL lists
 */
class Profile extends Model {

  /**
   * @var integer
   */
  public $id;

  /**
   * @var string
   */
  public $name;

  /**
   * Define relationships to User and Permission
   */
  public function initialize () {
    $this->hasMany('id', __NAMESPACE__ . '\User', 'profileId', array(
      'alias' => 'users',
      'foreignKey' => array(
        'message' => 'Profile cannot be deleted because it\'s used on User',
      ),
    ));

    $this->hasMany('id', __NAMESPACE__ . '\Permission', 'profileId', array(
      'alias' => 'permissions',
    ));
  }
}
