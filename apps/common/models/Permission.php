<?php

namespace Bicharka\Common\Models;

use Phalcon\Mvc\Model;

/**
 * Permission
 *
 * Stores the permissions by profile
 */
class Permission extends Model {

  /**
   * @var integer
   */
  public $id;

  /**
   * @var integer
   */
  public $profileId;

  /**
   * @var string
   */
  public $resource;

  /**
   * @var string
   */
  public $action;

  public function initialize () {
    $this->belongsTo('profileId', __NAMESPACE__ . '\Profile', 'id', array(
      'alias' => 'profiles',
    ));
  }
}
