<?php

namespace Bicharka\Common\Models;

use Phalcon\Mvc\Model;

/**
 * SuccessLogin
 *
 * This model registers successful logins registered users have made
 */
class SuccessLogin extends Model {

  /**
   * @var integer
   */
  public $id;

  /**
   * @var integer
   */
  public $userId;

  /**
   * @var string
   */
  public $ipAddress;

  /**
   * @var string
   */
  public $userAgent;

  public function initialize () {
    $this->belongsTo('userId', __NAMESPACE__ . '\User', 'id', array(
      'alias' => 'users',
    ));
  }
}
