<?php

return $frontendLoaderNamespaces = [
    'Bicharka\Frontend\Models'      => $this->config->application->modelsDir,
    'Bicharka\Frontend\Controllers' => $this->config->application->controllersDir,
    'Bicharka\Frontend\Forms'       => $this->config->application->formsDir,
];

