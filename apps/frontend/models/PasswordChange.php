<?php

namespace Bicharka\Models;

use Phalcon\Mvc\Model;

/**
 * PasswordChange
 *
 * Register when a user changes his/her password
 */
class PasswordChange extends Model {

  /**
   * @var integer
   */
  public $id;

  /**
   * @var integer
   */
  public $userId;

  /**
   * @var string
   */
  public $ipAddress;

  /**
   * @var string
   */
  public $userAgent;

  /**
   * @var integer
   */
  public $createdAt;

  public function initialize () {
    $this->belongsTo('userId', __NAMESPACE__ . '\User', 'id', array(
      'alias' => 'users',
    ));
  }

  /**
   * Before create the user assign a password
   */
  public function beforeValidationOnCreate () {
    // Timestamp the confirmaton
    $this->createdAt = time();
  }
}
