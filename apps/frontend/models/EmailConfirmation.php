<?php

namespace Bicharka\Models;

use Phalcon\Mvc\Model;

/**
 * EmailConfirmation
 *
 * Stores the reset password codes and their evolution
 */
class EmailConfirmation extends Model {

  /**
   * @var integer
   */
  public $id;

  /**
   * @var integer
   */
  public $userId;

  /**
   * @var string (char)
   */
  public $code;

  /**
   * @var integer
   */
  public $createdAt;

  /**
   *
   * @var integer
   */
  public $modifiedAt;

  /**
   * @var integer
   */
  public $confirmed;

  public function initialize () {
    $this->belongsTo('userId', __NAMESPACE__ . '\User', 'id', array(
      'alias' => 'users',
    ));
  }

  /**
   * Before create the user assign a password
   */
  public function beforeValidationOnCreate () {
    // Timestamp the confirmation
    $this->createdAt = time();

    // Generate a random confirmation code
    $this->code = preg_replace('/[^a-zA-Z0-9]/', '', base64_encode(openssl_random_pseudo_bytes(24)));

    // Set status to non-confirmed
    $this->confirmed = 0;
  }

  /**
   * Sets the timestamp before update the confirmation
   */
  public function beforeValidationOnUpdate () {
    // Timestamp the confirmation
    $this->modifiedAt = time();
  }

  /**
   * Send a confirmation e-mail to the user after create the account
   */
  public function afterCreate () {
    $this->getDI()
      ->getMail()
      ->send([
        $this->user->email => $this->user->email,
      ], "Please confirm your email", 'confirmation', [
        'confirmUrl' => '/confirm/' . $this->code . '/' . $this->user->email,
      ]);
  }
}
