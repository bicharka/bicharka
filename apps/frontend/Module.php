<?php

namespace Bicharka\Frontend;

use Phalcon\Config;
use Phalcon\DiInterface;
use Phalcon\Loader;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\ModuleDefinitionInterface;
use Phalcon\Mvc\View;

class Module implements ModuleDefinitionInterface {

  protected $config = null;

  public function __construct (){
    $commonConfig = include APP_DIR . '/common/config/config.php';
    $frontendConfig = include APP_DIR . '/frontend/config/config.php';

    $this->config = new Config(array_replace_recursive(
      $commonConfig,
      $frontendConfig
    ));
  }

  /**
   * Register a specific autoloader for the module
   */
  public function registerAutoloaders (DiInterface $di = null) {
    $loader = new Loader();

    $commonLoader = include APP_DIR . '/common/config/loader.php';
    $frontendLoader = include APP_DIR . '/frontend/config/loader.php';

    $loader->registerNamespaces(array_merge($commonLoader, $frontendLoader))->register();
  }

  /**
   * Register specific services for the module
   */
  public function registerServices (DiInterface $di) {
    include APP_DIR . '/frontend/config/services.php';
  }
}
