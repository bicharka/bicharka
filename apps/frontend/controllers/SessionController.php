<?php
namespace Bicharka\Frontend\Controllers;

use Bicharka\Frontend\Forms\LoginForm;
use Bicharka\Frontend\Forms\SignUpForm;
use Bicharka\Frontend\Forms\ForgotPasswordForm;
use Bicharka\Common\Library\Auth\Exception as AuthException;
use Bicharka\Common\Models\User;
use Bicharka\Common\Models\ResetPassword;

/**
 * Controller used handle non-authenticated session actions like login/logout, user signup, and forgotten passwords
 */
class SessionController extends ControllerBase {

  /**
   * Default action. Set the clean layout (layouts/clean.html)
   */
  public function initialize () {
    $this->view->setTemplateBefore('clean');
  }

  /**
   * Allow a user to signup to the system
   */
  public function signupAction () {
    $this->view->form = $form = new SignUpForm();

    if ($this->request->isPost()) {
      if ($form->isValid($this->request->getPost())) {

        $user = new User();
        $user->assign(array(
          'firstName' => $this->request->getPost('firstName', 'striptags'),
          'lastName' => $this->request->getPost('lastName', 'striptags'),
          'email' => $this->request->getPost('email'),
          'password' => $this->security->hash($this->request->getPost('password')),
          'profileId' => 2,
        ));

        if ($user->save()) {
          // todo add return statement if required
          $this->dispatcher->forward(array(
            'controller' => 'index',
            'action' => 'index',
          ));
        }

        $this->flash->error($user->getMessages());
      }
    }
  }

  /**
   * Starts a session in the admin backend
   */
  public function loginAction () {
    $this->view->form = $form = new LoginForm();
    $this->view->setTemplateBefore('clean');

    try {
      if (!$this->request->isPost()) {
        if ($this->auth->hasRememberMe()) {
          return $this->auth->loginWithRememberMe();
        }
      } else {
        if (!$form->isValid($this->request->getPost())) {
          foreach ($form->getMessages() as $message) {
            $this->flash->error($message);
          }
        } else {
          $this->auth->check([
            'email' => $this->request->getPost('email'),
            'password' => $this->request->getPost('password'),
            'remember' => $this->request->getPost('remember'),
          ]);

          $this->dispatcher->forward(array(
            'controller' => 'index',
            'action' => 'index',
          ));
        }
      }
    } catch (AuthException $e) {
      $this->flash->error($e->getMessage());
    }
  }

  /**
   * Shows the forgot password form
   */
  public function forgotPasswordAction () {
    $this->view->form = $form = new ForgotPasswordForm();

    if ($this->request->isPost()) {

      if ($form->isValid($this->request->getPost()) == false) {
        foreach ($form->getMessages() as $message) {
          $this->flash->error($message);
        }
      } else {

        $user = User::findFirstByEmail($this->request->getPost('email'));
        if (!$user) {
          $this->flash->success('There is no account associated to this email');
        } else {

          $resetPassword = new ResetPassword();
          $resetPassword->usersId = $user->id;
          if ($resetPassword->save()) {
            $this->flash->success('Success! Please check your messages for an email reset password');
          } else {
            foreach ($resetPassword->getMessages() as $message) {
              $this->flash->error($message);
            }
          }
        }
      }
    }
  }

  /**
   * Closes the session
   */
  public function logoutAction () {
    $this->auth->remove();

    return $this->response->redirect('index');
  }
}
