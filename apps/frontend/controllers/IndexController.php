<?php

namespace Bicharka\Frontend\Controllers;

/**
 * Display the default index page
 */
class IndexController extends ControllerBase {

  /**
 * Default action. Set the public layout (layouts/public.phtml)
 */
  public function indexAction () {
    $this->view->setVar('logged_in', is_array($this->auth->getIdentity()));
    $this->view->setTemplateBefore('public');
  }

  /**
   * Default administration action. Set the public layout (layouts/admin.phtml)
   */
  public function adminAction () {
    $this->view->setVar('logged_in', is_array($this->auth->getIdentity()));
    $this->view->setTemplateBefore('admin');
  }

}
